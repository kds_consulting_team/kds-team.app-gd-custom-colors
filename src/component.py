'''
Template Component main class.

'''

from kbc.env_handler import KBCEnvHandler
import logging
import sys
import json
import requests
import os

KEY_COLORS = "colors"
KEY_INCLUDE_DEFAULT = "include_default"
KEY_CONFIG_ID = "config_id"
KEY_PROJECT_ID = 'project_id'
KEY_SYRUP_URL = 'syrup_url'

PAR_DIRPATH = os.path.dirname(os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir)))
DEFAULT_HEX_FILE_PATH = os.path.join(PAR_DIRPATH, "default_hex.txt")

MANDATORY_PARS = [KEY_PROJECT_ID, KEY_CONFIG_ID]
MANDATORY_IMAGE_PARAMS = [KEY_SYRUP_URL]

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get('debug'):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARAMS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self, debug=False):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        project_id = params[KEY_PROJECT_ID]
        config_id = params[KEY_CONFIG_ID]
        include_default = params[KEY_INCLUDE_DEFAULT]
        colors = params[KEY_COLORS]
        storage_token = self.get_storage_token()
        syrup_url = self.get_image_parameters()[KEY_SYRUP_URL]

        try:
            listing = self.color_prep(colors, include_default)
            ch_info = self.put_gd(storage_token, project_id, config_id, listing, syrup_url)
            logging.info(ch_info)
            logging.info("------------------------")
            result = json.loads(ch_info)
            if result.get("status") == 'error':
                logging.error('Update request failed. Status code: %s, Message: %s',
                              result.get('code'), result.get('message'))
                sys.exit(1)
            logging.info("Job done.")

        except ValueError as err:
            logging.error(err)
            sys.exit(1)

        except Exception as err:
            logging.error(err)
            # traceback.print_exc(file=sys.stderr)
            sys.exit(2)

    def put_gd(self, storage, project, config_id, codes, syrup_url):
        """
        Request color change via GD writer login
         - Codes are in final JSON format based on DG description
         - identification from config: (storage, project, config(name of the writer))
        """

        json_string = '{"uri": "/gdc/projects/' + project + '/styleSettings/","payload":' + json.dumps(codes) + '}'
        # doing there and back excercise to make sure the formatting works
        parsed_json = json.loads(json_string)
        values = json.dumps(parsed_json)
        logging.debug(values)

        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': storage
        }

        # Colors change
        url_change = (syrup_url + "/gooddata-writer/v2/" + config_id + "/proxy")
        change_response = requests.put(url_change, headers=headers, data=values)

        logging.info("Change request response: " + str(change_response.status_code))
        logging.debug("------------------------")
        change_info = change_response.text
        logging.debug(change_response.headers)
        logging.debug(json.dumps(change_info, indent=2))

        return change_info

    def default_hex(self):
        """
        Get default TXT config for colors in GD - HEX LIST
        """

        j = open(DEFAULT_HEX_FILE_PATH)
        f = j.read()
        j.close()

        # Sending back just a string to keep the same formatting as user input
        # data = f.split(",")
        data = str(f)

        return data

    def hex_to_int_color(self, v):
        """
        Convert color from HEX -} LIST
        """

        if v[0] == '#':
            v = v[1:]
        assert (len(v) == 6)
        hex_tuple = int(v[:2], 16), int(v[2:4], 16), int(v[4:6], 16)
        return list(hex_tuple)

    def int_to_hex_color(self, v):
        """
        ! UNUSED !
        Convert color from RGB LIST -} HEX
        """

        assert (len(v) == 3)
        return '#%02x%02x%02x' % v

    def color_prep(self, colors_list, include_default):
        """
        Assemble JSON inject piece:
         - Merge default colors with custom ones
         - Create list of RGB codes for each color
         - Create color list
         - Create JSON piece
        """

        # Format user user input as a string for quick merge and unified conversion
        colors = ','.join(str(x) for x in colors_list)
        # Call default values and merge them with user input
        if include_default == "YES":
            default = self.default_hex()
            list_merge = default + "," + colors
        else:
            list_merge = colors

        list_custom = list_merge.split(",")
        logging.info("Selected colors: " + str(list_custom))
        logging.info("------------------------")

        # Create list of RGB codes for each color
        color_list = []
        num_id = 0
        for a in list_custom:
            rgb_codes = self.hex_to_int_color(a)
            # print rgb_codes
            piece_r = rgb_codes[0]
            piece_g = rgb_codes[1]
            piece_b = rgb_codes[2]
            num_id = num_id + 1
            guid = "guid" + str(num_id)
            # print guid
            piece_color = {"guid": guid,
                           "fill": {
                               "r": piece_r,
                               "g": piece_g,
                               "b": piece_b
                           }
                           }
            # print piece_color
            color_list.append(piece_color)

        # Create JSON piece
        json_piece = {
            "styleSettings": {
                "chartPalette": color_list
            }
        }

        return json_piece


"""
        Main entrypoint
"""
if __name__ == "__main__":
    comp = Component()
    comp.run()
